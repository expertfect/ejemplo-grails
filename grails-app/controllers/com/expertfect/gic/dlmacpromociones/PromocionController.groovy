package com.expertfect.gic.dlmacpromociones

import java.text.SimpleDateFormat
import java.util.Date;

class PromocionController {
	
	def static tiposPromociones = [
		[ id: 1, nombre: "Por partida" ],
		[ id: 2, nombre: "En base a ventas" ]
	]
	
	def static tiposBase = [
		[ id: 1, nombre: "Cantidad" ],
		[ id: 2, nombre: "Importe" ]
	]
	
	def static tiposAplicacion = [
		[ id: 1, nombre: "Porcentaje de descuento" ],
		[ id: 2, nombre: "Precio" ]
	]
	
	def static tiposEstado = [
		[ id: 1, nombre: "Activo" ],
		[ id: 2, nombre: "Aplicado" ],
		[ id: 3, nombre: "Inactivo" ]
	]

    def index() {
		return [ promociones: Promocion.list(sort: "nombre") ]
	}
	
	def crear() {
		
	}
	
	def guardar() {
		def promocion = new Promocion()
		
		promocion.promocion_id = params.promocion_id
		promocion.nombre = params.nombre
		promocion.tipo = params.int("tipo")
		promocion.tipoBase = params.int("tipoBase")
		promocion.tipoAplicacion = params.int("tipoAplicacion")
		promocion.grupo = params.grupo && params.grupo.length ? params.grupo : null
		promocion.estado = 1
		
		def formato = new SimpleDateFormat("dd/MM/yyyy")
		
		try {
			promocion.fechaVigenciaInicial = formato.parse(params.fechaVigenciaInicial)
			promocion.fechaVigenciaFinal = formato.parse(params.fechaVigenciaFinal)
			
			if (promocion.save()) {
				flash.exito = "Se ha guardado la promoción"
				redirect(action: "index")
			} else {
				render(view: "crear", model: [ promocion: promocion ])
			}
		} catch (ex) {
			ex.printStackTrace()
			flash.error = "Alguna de las fechas establecidas no es correcta"
			render(view: "crear", model: [ promocion: promocion ])
		}
	}
	
	def editar() {
		
	}
	
	def eliminar() {
		
	}
	
}
