dataSource {
    pooled = true
    driverClassName = "net.sourceforge.jtds.jdbc.Driver"
    username = ""
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:jtds:sqlserver://10.0.0.184:1433/GIC_Promociones;User=sa;Password=as;"
			validationQuery="SELECT 1"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = ""
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            url = "jdbc:jtds:sqlserver://10.0.0.184:1433/GIC_Promociones;User=sa;Password=as;"
            pooled = true
            properties {
               maxActive = -1
               minEvictableIdleTimeMillis=1800000
               timeBetweenEvictionRunsMillis=1800000
               numTestsPerEvictionRun=3
               testOnBorrow=true
               testWhileIdle=true
               testOnReturn=true
               validationQuery="SELECT 1"
            }
        }
    }
}
