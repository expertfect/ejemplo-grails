class UrlMappings {

	static mappings = {
		"/"(controller: "home")
		
		"/promociones"(controller: "promocion")
		"/promociones/$action?/$id?"(controller: "promocion")
	}
}
