<%@page import="com.expertfect.gic.dlmacpromociones.PromocionController"%>

<g:render template="/common/header" />

<div class="page-header">
	<h1>Promociones</h1>
</div>

<g:link class="btn btn-primary pull-right" action="crear">Crear nueva</g:link>

<table class="table table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Tipo</th>
			<th>Tipo base</th>
			<th>Tipo aplicación</th>
			<th>Estado</th>
		</tr>
	</thead>
	<tbody>
		<g:each in="${ promociones }" var="promocion">
			<tr>
				<td>${ promocion.promocion_id }</td>
				<td>${ promocion.nombre }</td>
				<td>${ PromocionController.tiposPromociones.find { it.id == promocion.tipo }.nombre }</td>
				<td>${ PromocionController.tiposBase.find { it.id == promocion.tipoBase }.nombre }</td>
				<td>${ PromocionController.tiposAplicacion.find { it.id == promocion.tipoAplicacion }.nombre }</td>
				<td>${ PromocionController.tiposEstado.find { it.id == promocion.estado }.nombre }</td>
			</tr>
		</g:each>
	</tbody>
</table>

<g:render template="/common/footer" />
