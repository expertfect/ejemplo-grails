<%@page import="com.expertfect.gic.dlmacpromociones.PromocionController"%>

<g:render template="/common/header" />

<div class="page-header">
	<h1>Nueva promoción</h1>
</div>

<g:hasErrors bean="${ promocion }">
	<div class="alert alert-danger">
		<g:renderErrors bean="${ promocion }" as="list" />
	</div>
</g:hasErrors>

<g:form action="guardar">
	<div class="form-group">
		<label>ID</label>
		<input class="form-control" type="text" name="promocion_id" id="promocion_id" value="${ promocion?.promocion_id }" />
	</div>
	<div class="form-group">
		<label>Nombre</label>
		<input class="form-control" type="text" name="nombre" id="nombre" value="${ promocion?.nombre }" />
	</div>
	<div class="form-group">
		<label>Tipo</label>
		<g:select class="form-control" name="tipo" id="tipo" from="${ PromocionController.tiposPromociones }" optionKey="id" optionValue="nombre" value="${ promocion?.tipo }" />
	</div>
	<div class="form-group">
		<label>Tipo base</label>
		<g:select class="form-control" name="tipoBase" id="tipoBase" from="${ PromocionController.tiposBase }" optionKey="id" optionValue="nombre" value="${ promocion?.tipoBase }" />
	</div>
	<div class="form-group">
		<label>Tipo aplicación</label>
		<g:select class="form-control" name="tipoAplicacion" id="tipoAplicacion" from="${ PromocionController.tiposAplicacion }" optionKey="id" optionValue="nombre" value="${ promocion?.tipoAplicacion }" />
	</div>
	<div class="form-group">
		<label>Fecha vigencia inicial</label>
		<input class="form-control" type="text" name="fechaVigenciaInicial" id="fechaVigenciaInicial" value="" />
	</div>
	<div class="form-group">
		<label>Fecha vigencia final</label>
		<input class="form-control" type="text" name="fechaVigenciaFinal" id="fechaVigenciaFinal" value="" />
	</div>
	<div class="form-group">
		<label>Grupo</label>
		<input class="form-control" type="text" name="grupo" id="grupo" value="${ promocion?.grupo }" />
	</div>
	
	<button class="btn btn-primary" type="submit">Guardar</button>
</g:form>

<g:render template="/common/footer" />
