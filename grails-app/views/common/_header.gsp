<!DOCTYPE html>
<html>

	<head>
	
		<title>DLMAC - Promociones</title>
		
		<link rel="stylesheet" type="text/css" href="${ resource(dir: 'css', file: 'bootstrap.css') }" />
		
		<script type="text/javascript" src="${ resource(dir: 'js', file: 'bootstrap.js') }"></script>
		
	</head>
	
	<body>
		
		<div class="container">
			<g:if test="${ flash.error }">
				<div class="alert alert-danger">
					<p>${ flash.error }</p>
				</div>
			</g:if>
			
			<g:if test="${ flash.alerta }">
				<div class="alert alert-warning">
					<p>${ flash.alerta }</p>
				</div>
			</g:if>
			
			<g:if test="${ flash.notificacion }">
				<div class="alert alert-info">
					<p>${ flash.notificacion }</p>
				</div>
			</g:if>
			
			<g:if test="${ flash.exito }">
				<div class="alert alert-success">
					<p>${ flash.exito }</p>
				</div>
			</g:if>
			