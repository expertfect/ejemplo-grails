package com.expertfect.gic.dlmacpromociones

class Promocion {

	String promocion_id
	String nombre
	Short tipo
	Short tipoBase
	Short tipoAplicacion
	String grupo
	Date fechaVigenciaInicial
	Date fechaVigenciaFinal
	Short estado

    static constraints = {
		promocion_id(blank: false, unique: true, maxSize: 32)
		nombre(blank: false)
		grupo(nullable: true, blank: false)
    }
	
}
